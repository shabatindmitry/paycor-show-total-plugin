var userData, punchData, totalHours, punchStatus, lastPunchDisplayDate, timeAfterLastInPunch;

var PUNCH_REFRESH_TIMEOUT = 3000;

function init() {
	var target = $('#totalHours');
	target.empty();
	$('status-area').empty();
	$('.timesheet').hide();
	$('.punch-button').hide();
	overlay(true);

	var timestamp = new Date().getTime();
	var timeSheetURI = "https://secure.paycor.com/TIME/API/TimeSheet/$ClientId$/$EmployeeUid$?ViewSetting=0&_=" + timestamp;
	var userURI = "https://secure.paycor.com/employeeservice/employee/v1/List?%24inlinecount=allpages&_=" + timestamp;

	$.ajax({
		url: userURI,
		crossDomain: true,
		dataType: 'json',
		cache: false,
		error: function (xhr, status, error) {
			ajaxError(status, error);
		},
		success: function (data) {
			console.log('user', data);
			if (data) {
				if (data.Count == 1) {
					userData = data.Items[0];
					if (userData.ClientId && userData.EmployeeUid) {
						timeSheetURI = fillIDsFromUserData(timeSheetURI);
						$.ajax({
							url: timeSheetURI,
							crossDomain: true,
							dataType: 'json',
							cache: false,
							headers: {
								"PaycorUseLongToStringConversion": "true"
							},
							error: function (xhr, status, error) {
								ajaxError(status, error);
							},
							success: function (data) {
								processData(data);
								renderPopup();
							}
						});
					}
				} else {
					unexpectedDataError();
				}
			} else {
				// cookies are missing?
				openPayCorTab();
			}
		}
	});
}

function sendPunch() {
	overlay(true);
	function sendPunchRequest() {
		var uri = "https://api.paycor.com/time/v2/punches";
		var reqData;
		var time = new Date();
		reqData = {
			activity: "work",
			clientId: userData.ClientId,
			departmentId: "",
			displayTime: getPunchRequestDisplayTime(time),
			employeeId: userData.EmployeeId,
			isTransfer: false,
			laborAssociations: [],
			note: "",
			period: getAMPM(time),
			source: "Portal",
			status: "auto"
		};
		console.log('reqData', reqData);

		chrome.tabs.query({
			url: '*://hcm.paycor.com/*'
		}, function(results) {
			function executeContentScripts(tab, closeWhenDone) {
				chrome.tabs.executeScript(tab.id, {file: "jquery-3.4.1.min.js"}, function(){
					chrome.tabs.executeScript(tab.id, {file: "content.js"}, function(){
						chrome.tabs.sendMessage(tab.id, {
							action: 'punch',
							data: reqData,
							uri: uri,
							close: closeWhenDone,
							tabId: tab.id
						});
					});
				});
			}
			if (results.length) {
				executeContentScripts(results[0], false);
			} else {
				chrome.tabs.create({
					url: 'https://hcm.paycor.com/Portal/#/',
					active: false
				}, function(tab) {
					executeContentScripts(tab, true);
				});
			}
		});
	}

	if (userData) {
		if (userData.EmployeeId) {
			sendPunchRequest();
		} else {
			fetchEmployeeId(sendPunchRequest);
		}
	} else {
		unexpectedDataError();
	}


}

function fetchEmployeeId(callback) {
	var uri = "https://secure.paycor.com/performapi/api/Employee/ReducedFields/$ClientId$/$EmployeeUid$";
	if (userData) {
		uri = fillIDsFromUserData(uri);
		$.ajax({
			url: uri,
			crossDomain: true,
			dataType: 'json',
			cache: false,
			error: function (xhr, status, error) {
				ajaxError(status, error);
			},
			success: function (data) {
				console.log('employee data', data);
				userData.EmployeeId = data.EmployeeId;
				if (typeof callback === 'function') {
					callback();
				} else {
					return userData.EmployeeId;
				}

			}
		});
	} else {
		unexpectedDataError();
	}
}

function fillIDsFromUserData(uri) {
	$.each(['ClientId', 'EmployeeUid', 'EmployeeId'], function (i, val) {
		uri = uri.replace('$' + val + '$', userData[val]);
	});
	return uri;
}


function processData(data) {
	if (data) {
		console.log('timesheet data', data);
		punchData = data.TimeCardDays;
		var prevHours = data.Summary.HourTotal;
		var reversed = Array.prototype.slice.call(punchData);
		reversed.reverse();
		totalHours = Math.round(prevHours * 100) / 100;
		console.log('current hours as reported by paycor', prevHours);

		$.each(reversed, function (i, val) {
			if (dateIsToday(val.DisplayDate)) {
				console.log('today\'s timecard entry', val);
			}
			if (val.PunchPairList.length) {
				console.log('latest day with punches', val);
				var lastPunchPair = val.PunchPairList[val.PunchPairList.length - 1];
				console.log('last punch pair', lastPunchPair);
				if (lastPunchPair.InPunchDateTime) {
					if (!lastPunchPair.OutPunchDateTime) {
						punchStatus = true;
						lastPunchDisplayDate = lastPunchPair.DisplayInRoundedPunchDateTime;
						var lastPunch = new Date(lastPunchPair.InPunchDateTime).getTime();
						var delta = (new Date().getTime() - lastPunch) / 1000 / 3600;
						timeAfterLastInPunch = delta;
						totalHours = Math.round((prevHours + delta) * 100) / 100;
					} else {
						// prevHours is already correct number, since user is clocked out currently
						punchStatus = false;
						lastPunchDisplayDate = lastPunchPair.DisplayOutRoundedPunchDateTime;
						totalHours = Math.round(prevHours * 100) / 100;
					}
				} else {
					unexpectedDataError();
				}
				return false;
			}
		});
	}
}

function renderPopup() {
	renderTotalHours();
	renderTimesheet(null, true);
	renderStatus();
	renderPunchButton();
	overlay(false);
}

function renderTotalHours() {
	var total = totalHours;
	console.log('rounded total', total);
	$('#totalHours').text(total);
}

function renderStatus() {
	var clockedIn = punchStatus;
	clockedIn = typeof clockedIn == 'undefined' ? false : clockedIn;
	var statusContent = $('<span class="status"><span class="' + 'status-' + (clockedIn ? 'in' : 'out') + '">' + (clockedIn ? 'in' : 'out') + '</span><span> at ' + lastPunchDisplayDate + '</span></span>');
	$('.status-area').html(statusContent);
}

function renderTimesheet(selectedDay, init) {
	$('.timesheet').show();
	renderSummaryTable(selectedDay);
	renderPunchCard(selectedDay, init);
}

function renderSummaryTable(selectedDay) {
	var dayCells = [];
	$.each(punchData, function (i, val) {
		//var width = 100 / punchData.length * 2;
		//width = Number(width.toPrecision(5));
		var weekday = getDayOfTheWeek(val.DisplayDate);
		var selected = false;
		if ((selectedDay && selectedDay === val.DisplayDate) || (!selectedDay && dateIsToday(val.DisplayDate))) {
			selected = true;
		}
		if ($.inArray(weekday, ['Sun', 'Sat']) === -1) {
			var day = getDayOfTheMonth(val.DisplayDate).toString();
			var suffix = 'th';
			var hours = val.DailyTotals.TotalHours;
			if (dateIsToday(val.DisplayDate) && timeAfterLastInPunch) {
				hours += timeAfterLastInPunch;
			}
			switch (day[day.length - 1]) {
				case '1':
					suffix = 'st';
					break;
				case '2':
					suffix = 'nd';
					break;
				case '3':
					suffix = 'rd';
					break;
			}
			// suffix not used as of now
			var cell = $('<div class="day-cell' + (selected ? ' selected' : '') + (dateIsToday(val.DisplayDate) ? ' today' : '') + '" data-date="' + val.DisplayDate + '"></div>');
			cell.append($('<span class="weekday">' + weekday + '</span>'));
			cell.append($('<span class="date">' + day + '</span>'));
			cell.append($('<span class="total-hours">' + Number(hours.toPrecision(3)) + ' h</span>'));
			dayCells.push(cell);
		}
	});
	$('.summary-table').html(dayCells);
}

function renderPunchCard(selectedDay, init) {
	var selectedCard;
	$.each(punchData, function (i, val) {
		if (selectedDay && selectedDay === val.DisplayDate) {
			selectedCard = val;
		} else if (init && dateIsToday(val.DisplayDate)) {
			selectedCard = val;
		}
	});
	if (selectedCard) {
		var rows = [];
		$('.punch-card .header').html($('<span class="right">' + selectedCard.DisplayDate + '</span><span class="left">' + selectedCard.FirstLastPunch + '</span>'));
		if (selectedCard.PunchPairList) {
			$.each(selectedCard.PunchPairList, function (i, data) {
				$.each(['In', 'Out'], function (i, which) {
					if (data[which + 'PunchDateTime']) {
						rows.push($('<div class="punch-row"><div class="col-which">' + which + '</div><div class="col-time">' + data['Display' + which + 'RoundedPunchDateTime'] + '</div></div>'));
					}
				});
			});
		}
		if (rows.length) {
			$('.punch-table').html(rows);
		} else {
			$('.punch-table').html('<span class="no-punches">No punches yet</span>');
		}
		$('.punch-card').show();
	} else {
		$('.punch-card').hide();
	}
}

function renderPunchButton() {
	var word = null;
	if (typeof punchStatus !== 'undefined') {
		word = punchStatus ? 'out' : 'in';
	}
	$('.punch-button').show().html('Punch' + (word ? ' ' + word : ''));
}

function renderOverlayMessage(msg, error) {
	$('.msg').html(msg).addClass(error?'error':'success');
	setTimeout(clearOverlayMessage, PUNCH_REFRESH_TIMEOUT);
}

function clearOverlayMessage() {
	$('.msg').empty().removeClass('error success');
}


function openPayCorTab() {
	// redirect to paycor to log in
	//check if paycor tab open. If so, switch tabs, if not open a new tab
	// for now just displaying a link
	var loginLink = "https://hcm.paycor.com/accounts/Authentication/Signin?ReturnUrl=https%3A%2F%2Fhcm.paycor.com%2FPortal%2F%23%2F";
	overlay(false);
	$('main').empty().html('<h2>Please <a href="' + loginLink + '" target="_blank">log in</a> to Paycor for this extension to work</h2>');
}

function getCurrentDate() {
	var dateObj = new Date();
	var month = dateObj.getMonth() + 1; //months from 1-12
	var day = dateObj.getDate();
	var year = dateObj.getFullYear();

	return year + "-" + month + "-" + day;
}

function dateIsToday(date) {
	return new Date(date).getTime() == new Date(new Date().toDateString()).getTime();
}

function getDayOfTheWeek(date) {
	return new Date(date).toLocaleTimeString('en-us', {weekday: 'short'}).split(' ')[0];
}

function getDayOfTheMonth(date) {
	var date = new Date(date).getDate();
	return date;
}

function getFullDate(date) {
	return new Date(date).toLocaleTimeString('en-us', {
		weekday: 'long',
		year: 'numeric',
		month: 'long',
		day: 'numeric'
	});
}

function getPunchRequestDisplayTime(time) {
	// formatting datetime string into paycor's weird format
	if (typeof time === 'undefined') {
		time = new Date();
	}
	var pad = function (num) {
		var norm = Math.floor(Math.abs(num));
		return (norm < 10 ? '0' : '') + norm;
	};
	return time.getFullYear() +
		'-' + pad(time.getMonth() + 1) +
		'-' + pad(time.getDate()) +
		'T' + pad(time.getHours()) +
		':' + pad(time.getMinutes()) +
		':' + pad(time.getSeconds()) + '.000+00:00';
}

function getAMPM(time) {
	if (typeof time === 'undefined') {
		time = new Date();
	}
	return time.getHours() >= 12 ? 'PM' : 'AM';
}

function unexpectedDataError() {
	alert('Oops, unexpected data fetched from paycor. Please contact developer for a fix.');
}

function ajaxError(status, error) {
	console.log('ajax error: ', status, error);
	if (error === 'Unauthorized') {
		openPayCorTab();
	}
}

function overlay(toggle) {
	if (toggle) {
		$('.load-overlay').show(100);
	} else {
		$('.load-overlay').hide(100);
		clearOverlayMessage(); // just in case
	}
}

$(function () {
	init();

	chrome.runtime.onMessage.addListener(function (request, sender, callback) {
		if (request.action === "punchRequestComplete") {
			console.log('punch complete response', request);
			if (request.data) {
				if (request.data.status === 'success') {
					renderOverlayMessage('Punch successfully submitted. Waiting 3 seconds to refresh.');
				} else if (request.data.status === 'badOASK') {
					renderOverlayMessage('Punch NOT processed. Please try again.', true);
				} else {
					renderOverlayMessage('Punch was not processed. Please double check your punch status in actual Paycor website and submit a bug report to the extension developer.', true);
				}
				setTimeout(init, PUNCH_REFRESH_TIMEOUT);
			} else {
				unexpectedDataError();
			}
		}
	});

	$('main').on('click', '.refresh', function () {
		init();
	});

	$('main').on('click', '.day-cell', function () {
		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
			renderPunchCard();
		} else {
			$('.day-cell').removeClass('selected');
			$(this).addClass('selected');
			renderPunchCard($(this).data('date'));
		}
	});
	$('main').on('click', '.punch-button', function () {
		sendPunch();
	});
})
