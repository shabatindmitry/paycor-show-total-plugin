
var OASK;

console.log('CONTENT SCRIPT');

chrome.runtime.onMessage.addListener(function (request, sender, callback) {
	if (request.action === "punch") {
		console.log('message received', request);

		var reg = /"([a-f0-9]{32})"/g;

		function getBundleFileURL() {
			return $('script[src]').filter(function() {
				return $(this).attr('src').indexOf('assets/portal.bundle') !== -1;
			}).attr('src');
		}

		function submitPunch() {
			$.ajax({
				url: request.uri,
				crossDomain: true,
				data: JSON.stringify(request.data),
				contentType: 'application/json',
				cache: false,
				method: 'POST',
				headers: {
					'Ocp-Apim-Subscription-Key': OASK
				},
				xhrFields: {
					withCredentials: true
				},
				error: function (xhr, status, error) {
					ajaxError(status, error);
					if (request.close === true) {
						chrome.runtime.sendMessage({
							action: 'closeTab',
							tabId: request.tabId
						});
					}
				},
				success: function (data) {
					console.log('punch response', data);
					if (data.status) {
						chrome.runtime.sendMessage({
							action: 'punchRequestComplete',
							data: data
						});
					} else {
						customError(2);
					}
					if (request.close === true) {
						chrome.runtime.sendMessage({
							action: 'closeTab',
							tabId: request.tabId
						});
					}
				}
			});
		}

		function fetchOASKAndSubmit() {
			$.ajax({
				url: getBundleFileURL(),
				cache: false,
				xhrFields: {
					withCredentials: true
				},
				error: function (xhr, status, error) {
					ajaxError(status, error);
				},
				success: function (data) {
					var matches = reg.exec(data);
					console.log('matches', matches);
					if (matches.length || matches[1].length) {
						OASK = matches[1];
						chrome.storage.local.set({OASK: OASK}, function() {
							console.log('Setting OASK in localStorage to ' + OASK);
							submitPunch();
						});
					} else {
						customError(1);
					}
				}
			});
		}

		function getOASKAndSubmit() {
			if (OASK && OASK.length) {
				submitPunch();
			} else {
				chrome.storage.local.get(['OASK'], function(result) {
					console.log('OASK from localStorage ' + result.OASK);
					if (result.OASK && result.OASK.length) {
						OASK = result.OASK;
						submitPunch();
					} else {
						fetchOASKAndSubmit();
					}
				});
			}
		}

		getOASKAndSubmit();
		return true;
	}
});

function ajaxError(status, error) {
	console.log('ajax error: ', status, error);
	switch (error) {
		case 'Access Denied':
			chrome.storage.local.set({OASK: null}, function() {
				console.log('Unsetting OASK in localStorage.');
				chrome.runtime.sendMessage({
					action: 'punchRequestComplete',
					data: {
						status: 'badOASK'
					}
				});
			});
			break;
		case 'Unauthorized':
			openPayCorTab();
			break;
	}
}

function customError(code) {
	alert('Error #' + code + '. Please notify developer to help resolve the issue.');
}

function openPayCorTab() {
	// redirect to paycor to log in
	//check if paycor tab open. If so, switch tabs, if not open a new tab
	// for now just displaying a link
	var loginLink = "https://hcm.paycor.com/accounts/Authentication/Signin?ReturnUrl=https%3A%2F%2Fhcm.paycor.com%2FPortal%2F%23%2F";
	$('main').empty().html('<h2>Please <a href="' + loginLink + '" target="_blank">log in</a> to Paycor for this extension to work</h2>');
}
