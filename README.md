# Paycor Widget Chrome Extension

## Installation
1. Download `chrome-paycor-show-total.crx`
2. Open chrome extensions page
3. In top right, toggle Developer Mode ON
4. Drag the `chrome-paycor-show-total.crx` file to the chrome extensions window

## Usage
1. Make sure you are logged in to PayCor. You might want to update your settings to stay logged in for as long as possible.
2. Click the "P" icon in top right corner (in the extension toolbar), to see your current hours.
3. Enjoy!

## TODO
* Improve error handling
* Not sure if this will work for managers
* Handle cases where user hasn't made a punch this day yet
* Handle missed punch request cases
* Deal with the `SameSite` cookie attribute problem

## Potential Features
* Allow punching in and out from the popup window

